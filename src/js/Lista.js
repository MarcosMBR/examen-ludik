import React from 'react';
import Pelicula from './Pelicula';

function Lista(props) {
    return(
        <div className="List">
            {
                props.items.map(pelicula =>
                    <Pelicula
                        key = {pelicula.id}
                        id = {pelicula.id}
                        titulo = {pelicula.titulo}
                        imagen = {pelicula.imagen}
                        descripcion = {pelicula.descripcion}
                        estrellas = {pelicula.estrellas}
                    />
                )
            }
        </div>
        
    );
}
export default Lista;