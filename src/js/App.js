import React from 'react';
import logo from '../img/logo.svg';
import '../css/App.css';
import Menu from './Menu';
import Lista from './Lista';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listaPeliculas: [
        { id: 0, estrellas: 4, titulo: 'Piratas del Caribe', descripcion: 'sdklad', imagen: 'piratas.jpg' },
        { id: 1, estrellas: 3, titulo: 'Avengers', descripcion: 'sdad', imagen: 'avengers.jpg' },
        { id: 2, estrellas: 5, titulo: 'Whiplash', descripcion: 'pasdpasd dpasd pa', imagen: 'whiplash.jpg' },
        { id: 3, estrellas: 5, titulo: 'Batman', descripcion: '', imagen: 'batman.jpg' },
        { id: 4, estrellas: 5, titulo: 'Soy leyenda', descripcion: '', imagen: 'soyleyenda.jpg' },
        { id: 5, estrellas: 5, titulo: 'Harry Potter', descripcion: '', imagen: 'harrypotter.jpg' },
        { id: 6, estrellas: 5, titulo: 'Duro de matar', descripcion: '', imagen: 'duromatar.jpg' },
        { id: 7, estrellas: 5, titulo: 'El hombre araña', descripcion: '', imagen: 'spiderman.jpg' }
      ],
      copiaListaPeliculas: []
    };
  }

  componentDidMount() {
    this.iniciarPeliculas();
  }

  iniciarPeliculas = () => {
    this.setState((state, props) => ({
      copiaListaPeliculas: [...state.listaPeliculas]
    }));
  }

  onAdd = (item) => {
    var arregloTemporal = [... this.state.books];
    var peliculaNueva = {};

    this.obtenerPeliculas();

    peliculaNueva.id = arregloTemporal[arregloTemporal.length - 1].id + 1;
    peliculaNueva.estrellas = item.estrellas;
    peliculaNueva.titulo = item.titulo;
    peliculaNueva.imagen = item.imagen;

    arregloTemporal.push(peliculaNueva);
    this.setState({ listaPeliculas: [...arregloTemporal] });
    this.setState({ copiaListaPeliculas: [...arregloTemporal] });
  }

  obtenerPeliculas = (palabraBuscar) => {
    return "";
  }

  render() {
    return (
      <div className="app">
        <Menu titulo="Cine Ludik" onAdd={this.onAdd} onBuscar={this.obtenerPeliculas}/>
        <div align="right">
          <br/>
          <button onClick={this.obtenerPeliculas} className="button btn-green">+ Añadir pelicula</button>
        </div>
        <Lista items={this.state.copiaListaPeliculas} />
      </div>
    );
  }
}

export default App;
