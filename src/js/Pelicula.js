import React from 'react';
import '../css/App.css';
import '../css/Pelicula.css';
import DetallePelicula from '././modal/DetallePelicula';

class Pelicula extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            estrellas: [],
            abrirDetalle: false
        };
    }

    componentDidMount(){
        this.setState({
            estrellas: Array(parseInt(this.props.estrellas)).fill(0)
        });
    }

    cerrarDetallePelicula = () => {
        this.setState({
            abrirModal: false
        });
    }

    verDetallePelicula = (e) => {
        this.setState({
            abrirModal: true
        });
        e.preventDefault();
        const titulo = this.state.titulo;
        const imagen = this.state.imagen;
        const descripcion = this.state.descripcion;
        const rating = this.state.rating;
        this.props.onSet({titulo: titulo, imagen: imagen, descripcion: descripcion, rating: rating});
    }

    render(){
        return(
            <div className="pelicula" onClick={this.verDetallePelicula}>
                <div className="image" ><img src={'/img/' + this.props.imagen} width="100%"/></div>
                <div className="title">{this.props.titulo}</div>
                <div className="rating">
                    <p>
                        {
                            this.state.estrellas.map( i =>
                                <img src="img/star.png" width="32"/>
                            )
                        }
                    </p>
                    Clasificación
                    <select value={this.props.estrellas}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div className="actions">
                    <button>Eliminar</button>
                </div>
                {
                    this.state.abrirModal?
                    <DetallePelicula onCancel={this.cerrarDetallePelicula} onSet={this.onSet}/>
                    :
                    ''
                }
            </div>
        );
    }
}

export default Pelicula;