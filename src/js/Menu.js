import React from 'react';
import '../css/App.css';
import Busqueda from '././Busqueda';
//import ModalAgregarLibro from '././modal/ModalAgregarLibro';
import '../css/Menu.css';

class Menu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            abrirModal: false
        };
        // enlazamos la función a la clase
        this.agregarLibro = this.agregarLibro.bind(this);
        this.cerrarModalAgregarLibro = this.cerrarModalAgregarLibro.bind(this);
        this.buscar = this.buscar.bind(this);
    }

    agregarLibro() {
        this.setState({
            abrirModal: true
        });
    }

    cerrarModalAgregarLibro() {
        this.setState({
            abrirModal: false
        });
    }

    buscar() {
        console.log("HOLA");
    }

    render() {
        return (
            <div className="container">
                <div className="subcontainer">
                    <div className="logo">
                        {this.props.titulo}
                    </div>

                    <div className="search">
                        <Busqueda buscar={this.buscar}/>
                    </div>
                    
                    
                </div>
            </div>
        );
    }
}
export default Menu;