import React from 'react';

class Busqueda extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }



    render() {
        return (
            <div className="subcontainer">
                <input type="text" onChange={this.props.buscar} name="palabraBuscar" className="input" />
                <div className="actions" style={{marginLeft: 10 + 'px', marginRight: 10 + 'px'}}>
                    <button onClick={this.agregarLibro} className="button btn-blue">Buscar</button>
                </div>
            </div>
        );
    }
}
export default Busqueda;