import React, {useEffect} from 'react';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            APIKEY: '070f10677149debe5e4549b15dfe4955', // Clave generada Marcos
            baseURL: 'https://api.themoviedb.org/3/',
            configData: null,
            baseImagenUR: null
        };
    }

    getConfig(palabraBuscar) {
        const APIKEY = this.state.APIKEY;
        const baseURL = this.state.baseURL;
        const configData = this.state.configData;
        const baseImagenURL = this.state.baseImagenURL;

        let url = "".concat(baseURL, 'configuration?api_key=', APIKEY);
        console.log("");
        console.log("");
        console.log("url --> ", url);
        console.log("");
        console.log("");
        fetch(url).then((result) => {
            console.log("result.json() --> ", result.json());
            return result.json();
        }).then((data) => {
            console.log("data --> ", data);
            baseImagenURL = data.images.secure_base_url;
            configData = data.images;
            this.buscar(palabraBuscar);
        }).catch(function (err) {
            alert(err)
        });
    }

    buscar(palabraBuscar) {
        const APIKEY = this.state.APIKEY;
        const baseURL = this.state.baseURL;
        let url = ''.concat(baseURL, 'search/movie?api_key=', APIKEY, '&query=', palabraBuscar);
        fetch(url)
            .then(result => result.json())
            .then((data) => {
                //document.getElementById('output').innerHTML = JSON.stringify(data, null, 4);
                console.log(JSON.stringify(data, null, 4));
            })
    }

    render() {
        return (
            <div className="app">
                <Menu titulo="Librería" onAdd={this.onAdd} />

                <button onClick={this.obtenerPeliculas} className="button btn-blue">Buscar pelicula</button>
                <Lista items={this.state.books} />
            </div>
        );
    }
}

export default App;