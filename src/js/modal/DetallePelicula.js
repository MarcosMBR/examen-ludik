import React from 'react';

class DetallePelicula extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.cancelAction = this.cancelAction.bind(this);
        this.onSet = this.onSet.bind(this);
    }

    cancelAction(e) {
        this.props.onhide();
    }

    cambioClasificacion = (e) => {
        e.preventDefault();
        const clasificacion = parseInt(e.target.value);
        //this.setState({clasificacion: clasificacion});
    }

    guardar = (e) => {
        e.preventDefault();
        this.props.onCancel();
    }

    onSet(item){
        console.log("aodknkasnd");
        console.log(item);
    }

    render() {
        return (
            <div className="new-item-panel-container">
                <div className="new-item-panel">
                    <div className="image" ><img src={'/img/' + this.props.imagen} width="100%" /></div>
                    <div className="title">{this.props.titulo}</div>
                    <div className="rating">
                        
                        Clasificación
                    <select value={this.props.estrellas}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>

        );
    }
}
export default DetallePelicula;